$(() => {
    $.getJSON('/data.json', (data) => {
        const d = data.display_data[getUrlParam("i")];
        const fileIndex = Math.max(getUrlParam("j") - 1, 0)

        if (!d) {
            $('#content').empty().text('暂无数据').css({
                textAlign: 'center',
                fontSize: 20,
                color: '#f00',
                display: 'block'
            })
            return
        }

        let ContentShow = $(`<div class="content_show"><iframe src="" frameborder="0"></iframe><div class="content_show-article"></div></div>`)
        $("#content").append(ContentShow)

        $(".content_menu-header span").text(d.title)
        for (const listElement of d.list) {
            let listItem = $(`<li class="content_menu-li" file_url="${listElement.url}"><div class="content_menu-li-face"><img class="content_menu-li-img" src="/images/1.png" />${listElement.title}</div></li>`)
            if (listElement.sub && listElement.sub.length > 0) {
                let subMenu = $(`<ul class="content_menu content_sub_menu"></ul>`)
                for (const subElement of listElement.sub) {
                    subMenu.append($(`<li class="content_menu-li content_sub_menu-li" file_url="${subElement.url}">${subElement.title}</li>`).on('click', (e) => {
                        ContentShow.find('.content_show-article').hide();
                        ContentShow.find('iframe').attr('src', e.currentTarget.getAttribute("file_url")).show();
                    }))
                }
                subMenu.css('display', 'none')
                listItem.append(subMenu)
                listItem.attr('litype', 'menu')
            }
            listItem.find('.content_menu-li-face').on('click', (e) => {
                if (e.currentTarget.parentElement.getAttribute("litype") === 'menu') {
                    $(e.currentTarget.parentElement).find(".content_sub_menu").slideToggle('slow')
                    return
                }
                if (e.currentTarget.parentElement.getAttribute("file_url") != "undefined") {
                    ContentShow.find('.content_show-article').hide();
                    ContentShow.find('iframe').attr('src', e.currentTarget.parentElement.getAttribute("file_url")).show();
                    return;
                }
                ContentShow.find('iframe').hide();
                ContentShow.find('.content_show-article').html(getArticleByTitle(e.currentTarget.parentElement.outerText)).show()
            })
            listItem.appendTo($(".content_menu-list"))
            $("<li class='content_menu-separate'>.................................</li>").appendTo($(".content_menu-list"))
        }
        if ($('.content_menu-li')[fileIndex].getAttribute("file_url") != "undefined") {
            ContentShow.find('.content_show-article').hide();
            ContentShow.find('iframe').attr('src', $('.content_menu-li')[fileIndex].getAttribute("file_url")).show()
        } else {
            ContentShow.find('iframe').hide();
            ContentShow.find('.content_show-article').html(getArticleByTitle($('.content_menu-li')[fileIndex].outerText)).show()
        }

        /**
         * 根据标题查找文章内容
         * @param title
         * @returns {*}
         */
        function getArticleByTitle(title) {
            for (let i = 0; i < d.list.length; i++) {
                let listElement = d.list[i]
                if (listElement.title === title) {
                    return listElement.article
                }
            }
        }
    })
})