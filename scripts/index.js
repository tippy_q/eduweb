$(() => {
    /**
     * 添加轮播图
     */



    function addBanner() {
        const bannerImages = [{
            imgSrc: '/images/banner.jpg',
            videoSrc: 'http://r5xyttnxb.hd-bkt.clouddn.com/%E2%80%9C%E4%B8%93%E4%B8%9A%E8%9E%8D%E5%90%88%E3%80%81%E5%AD%A6%E8%B5%9B%E7%A0%94%E5%88%9B%E7%BB%93%E5%90%88%E2%80%9D%E7%9A%84%E6%96%B0%E6%97%B6%E4%BB%A3%E9%AB%98%E8%81%8C%E5%95%86%E7%A7%91%E6%95%B0%E5%AD%A6%E8%AF%BE%E7%A8%8B%E5%BB%BA%E8%AE%BE%E4%B8%8E%E5%AE%9E%E8%B7%B5.mp4\n',
            "src": "/pages/display.html?i=cgjj&j=2"
        }]
        const gallery = $(".banner_gallery")
        // const dots = $(".banner_dots")
        for (let i = 0; i < bannerImages.length; i++) {
            let item = $(`<div class="banner_gallery-item"><a href="${bannerImages[i].src}"><img class="banner_gallery-img" src="${bannerImages[i].imgSrc}" alt="${bannerImages[i].desc || ''}"/></a><video src="${bannerImages[i].videoSrc}" autoplay controls loop/></div>`)
            gallery.append(item)
            // dots.append($(`<div class="banner_dots-item">${i + 1}</div>`))
        }
        startBanner()
    }

    /**
     * 运行轮播图
     */
    function startBanner() {
        //轮播图切换时间  秒
        const BANNER_TOGGLE = 5
        let bannerList = $('.banner_gallery-item')
        let current = 0
        renderBanner()
        let bannerInterval = setInterval(() => {
            current++
            renderBanner()
        }, BANNER_TOGGLE * 1000)

        // $('.banner_dots-item').each((e) => {
        //     $($('.banner_dots-item')[e]).on('mouseenter', () => {
        //         window.clearInterval(bannerInterval)
        //         bannerInterval = null
        //         current = e
        //         renderBanner()
        //         bannerInterval = setInterval(() => {
        //             current++
        //             renderBanner()
        //         }, BANNER_TOGGLE * 1000)
        //     })
        // })

        function renderBanner() {
            if (current > bannerList.length - 1) current = 0
            $('.banner .banner_gallery-item-active').removeClass('banner_gallery-item-active')
            $(bannerList[current]).addClass('banner_gallery-item-active')
            // $('.banner .banner_dots-item-active').removeClass('banner_dots-item-active')
            // $($('.banner_dots-item')[current]).addClass('banner_dots-item-active')
        }
    }

    /**
     * 添加首页内容
     */
    function addContent() {
        const contentData = [
            {
                title: '成果简介',
                content: `<img src="http://r5xyttnxb.hd-bkt.clouddn.com/%E6%96%87%E7%AB%A0%E5%AE%A3%E4%BC%A0%E5%9B%BE1.jpg" /><p>目前，高职数学教学普遍存在与专业割离，脱离专业教学，“边缘化”的问题，近年来，商科专业迅速发展，数学教学追不上专业快速发展的步伐，同时教学很难适应职业教育生源多样化的现状，并且缺乏对商科专业人才培养目标需要具备的创新素养、科技素养、数学建模、逻辑推理等数学学科素养的支持。</p><p>围绕上述问题，我们教学中进行积极地探索与实践。2011年起，依托“数学建模对高职创新人才培养”校级课题，开展“学赛研创”融合的教学改革实践；后续继续深入研究，依托省级课题“基于职业能力导向的高职工科高等数学微课开发与应用研究”，开展专业与数学教学融入的教学研究；校级课题“基于职教云的高职商科数学教学模式改革研究”，开展商科专业融合数学课程建设与实践。打造了专业融合、“学赛研创”结合的高职商科数学课程体系，为优化高等教育结构，服务商科专业人才培养，培养服务区域发展所需要的高素质技术技能人才提供了开创性思路和有力举措。学生竞赛获奖人次逐年增加，为商科学生就业和可持续发展奠定基础。</p><p>10年的研究与实践，学生的数学素养、创新能力、专业水平得到了全面的提升。学生竞赛获奖人次逐年增加。全国大学生数学建模竞赛累计获全国一等奖3项，二等奖5项，省级一等奖40余项；近三年参加省大学生数学竞赛，我校获奖人数平均每年提升5个百分点；近三年商科类技能大赛获省级以上奖励十余项。学生参加科研项目，具备初步科研能力...</p><p></p>`,
                url: '/pages/display.html?i=cgjj&j=1'
            },
            {
                title: '成果创新',
                content: `<img src="http://r5xyttnxb.hd-bkt.clouddn.com/%E6%96%87%E7%AB%A0%E5%AE%A3%E4%BC%A0%E5%9B%BE2.jpg" /><h3><p>&emsp;&emsp;（1）课程体系创新</p></h3><p>传统的数学教学往往只有理论的讲授，不注重专业的区分与融合，本成果在数学理论学习的基础上，结合职业教育的特点和新时代对于高职数学教学的要求，加入专业模块和提高课程，构建了满足不同层次和不同专业学生学习需求的“1+N+2”的数学课程体系，一方面能够满足不同层次和专业学生的学习需求，另一方面也使数学教学能够适应时代发展的需求。</p><h3><p>&emsp;&emsp;（2）教学理念创新</p></h3><p>2020年9月，教育部等九部门联合印发的《职业教育提质培优行动计（2020-2023）》中指出：巩固专科高职教育的主体地位，把发展专科高职教育作为优化高等教育结构的重要方式.本成果提出了以“服务专业”为导向，案例驱动、场景引入、模块化教学相结合的商科高职数学教学新理念，此理念明确数学的服务性，明确了高职商科数学教学中的手段和目标导向，为培养高素质技术技能人才提供了开创性思路...</p><b><h3><p>&emsp;&emsp;（3）教学模式创新</p></h3></b><p>本成果将专业融合放在了最突出的位置，同时采用学赛研创相结合的智慧教学模式，将专业知识和专业技能融入课堂教学，将数学建模思想和数据处理的实际案例融入课堂教学，并且借助信息技术使数学学习在时间和空间上得到了很大的延伸，取得了显著的成果。为职业教育实现提质培优、增值赋能和...</p>`,
                url: '/pages/display.html?i=cgjj&j=2'
            },
            {
                title: '建设成果',
                content: `<img src="http://r5xyttnxb.hd-bkt.clouddn.com/1%E5%9B%BE%E7%89%871.png" /><p>2011年10月开始建设“1+N+2”课程体系，课程体系主要分为以下三个部分：一是构建融合商科专业教师与数学教师组成的混合式教学研究团队，深入分析商科专业人才培养目标和专业课程体系，在此基础上分专业构建高职数学课程标准；二是构建了一套契合度高、适用性强的商科数学教学案例，利用实际专业案例...</p><div class="content_item-desc"><a href="/pages/display.html?i=cgjj&j=4">【详细】</a></div><hr /><img src="http://r5xyttnxb.hd-bkt.clouddn.com/%E5%9B%BE%E7%89%872%E5%BB%BA%E6%A8%A1%E8%8E%B7%E5%A5%96%E8%AF%81%E4%B9%A6.png" /><img src='http://r5xyttnxb.hd-bkt.clouddn.com/%E5%9B%BE%E7%89%872%E5%BC%A0%E5%BE%B7%E6%94%BF%E5%9C%A8%E4%BA%BA%E6%B0%91%E5%A4%A7%E4%BC%9A%E5%A0%82%E9%A2%86%E5%A5%96%E7%85%A7%E7%89%87.jpg' /><p>课程改革大大提升了育人成效。学生获全国大学生数学建模竞赛累计获一等奖3项，二等奖5项，省级一等奖40余项；近三年参加省大学生数学竞赛，我校获奖人数平均每年提升5个百分点；近三年商科类技能大赛获省级以上奖励十余项；近三年参加省大学生数学竞赛...</p>`,
                url: '/pages/display.html?i=cgjj&j=4'
            }
        ]
        const ContentTemplate = `<div class="content_item"><div class="content_item-header"></div><div class="content_item-main"></div><div class="content_item-desc"><a href="">【详细】</a></div></div>`;
        const content = $('#content')
        for (const cItem of contentData) {
            let c = $(ContentTemplate)
            c.find('.content_item-header').text(cItem.title)
            c.find('.content_item-main').html(cItem.content)
            c.find('.content_item-desc a').attr('href', cItem.url)
            content.append(c);
        }
    }

    /**
     * 添加成果展示内容
     */
    function addDisplay() {
        const displayData = [{img: 'https://imageproxy.chaoxing.com/0x0,jpeg,s0i_JCdxwhADKod74ATGW_u7R98G8JClLNWpnAPZ-I2g/https://p.ananas.chaoxing.com/star3/origin/419437bfe5e5bdad8e299fe34dea7832.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,skzXX2xj3cM4w1WUUGt-EFhhUAldiU_2EfkS8yQTOA_8/https://p.ananas.chaoxing.com/star3/origin/1d77811087b43f5abd4271599e8215f0.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,s47hVWg772fwG9PZPUJUTEtjWVmgqkckTWe918H-PfXg/https://p.ananas.chaoxing.com/star3/origin/44e10f21d2d44b57f8e815b8235abd4d.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,sQ-BOBKFfpFzHKHfNyKJ8aU4qyEwOkg6Ebm28g2ARUZE/https://p.ananas.chaoxing.com/star3/origin/29bb921158b029a280b57f0b8935e03d.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,s8zjs0zCHzZqxFZis16VRsyFaAxG6rif2UABDhYiBIcQ/https://p.ananas.chaoxing.com/star3/origin/46aeaaa62982d2dce4838f96d3583d22.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,sSPevkQTO-SXeMubmi5kpbjq7I4mJH5EddYiR2z__W9U/https://p.ananas.chaoxing.com/star3/origin/9cb311d1f4dbd53a36bf7a89457aaabd.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,shXf-h_aAOTbQ-wGAP7-pKUkQZ4XsOpmgPWd0rTYvtKw/https://p.ananas.chaoxing.com/star3/origin/e833cdaabe996c6b6654211cae819950.png'}, {img: 'https://imageproxy.chaoxing.com/0x0,jpeg,szKB1pE7jJLIg6sB7lTphpylvSLxMmDlTImQhbRwtt10/https://p.ananas.chaoxing.com/star3/origin/271df39413ecb48624716fc8bd677f4e.png'},{img:'http://imageproxy.chaoxing.com/0x0,jpeg,sAPsW0qsyhexFsPdNxo7_yKB2yVJY7LlR6beIxWQkQFY/https://p.ananas.chaoxing.com/star3/origin/0cb8849e408dfcd5e7c56fb628155e67.png'},{img:'https://imageproxy.chaoxing.com/0x0,jpeg,sogvKSZ6Ggtjz2RI-X9qiUl9HRS5JdrQUsRRc228Hb8k/https://p.ananas.chaoxing.com/star3/origin/7b75018e8a1a67ab933d74bfd58a664f.png'},{img:'https://imageproxy.chaoxing.com/0x0,jpeg,ssp5rvN_nT_Z7SYQeLUqOy21IpHZUYwuUd_p-t88bzNc/https://p.ananas.chaoxing.com/star3/origin/1b5940ff02e84dd70f94733730d9778f.png'},{img:'https://imageproxy.chaoxing.com/0x0,jpeg,s7ME5FxhSSSGlAWt-XDSV773Nxq6O12UJn-kUYkaSQ1A/https://p.ananas.chaoxing.com/star3/origin/96195e149e6542b861f6bc87efa5a23f.png'}]
        const gallery = $('#display .display_gallery')
        const imgStyle = {
            width: 214,
            height: 120,
            marginRight: 12,
            float: 'left'
        }

        let images = $(`<div class="display_images"></div>`)
        let num = getDisplayNum(displayData.length)
        for (let i = 0; i < displayData.length; i++) {
            let img = displayData[i % displayData.length].img
            images.append($(`<img class="display_main-item" src="${img}" />`).css(imgStyle));
        }
        gallery.append(images)

        startDisplay()
    }

    /**
     * 计算展示列表的数量
     */
    function getDisplayNum(num) {
        if (num > 4) return num
        let n = num
        do {
            num += n
        } while (num < 5)
        return num
    }

    /**
     * 开始滚动成果展示内容
     */
    function startDisplay() {
        const TIME = 30 * 1000
        const itemLength = $(".display_images .display_main-item").length
        let segmentWidth = itemLength * $(".display_images .display_main-item").outerWidth(true)
        for (let i = 0; i < getDisplayNum(itemLength) / itemLength; i++) {
            $(".display_images .display_main-item").clone().appendTo($(".display_images"));
        }
        run(TIME, segmentWidth)
        $('.display_gallery').on({
            mouseenter: () => {
                $('.display_images').stop()
            },
            mouseleave: () => {
                let passedCourse = -parseInt($('.display_images').css('left'))
                let time = TIME * (1 - passedCourse / segmentWidth)
                run(time, segmentWidth)
            }
        })

        function run(interval) {
            $(".display_images").animate({left: -segmentWidth}, interval, "linear", () => {
                $(".display_images").css("left", 0);
                run(TIME)
            })
        }
    }

    /**
     * 初始化
     */
    function init() {
        addBanner()
        addContent()
        addDisplay()
    }

    init()
})